# Libertarian Kazakhstan - Либертар Қазақстан

## description
Мы есть think tank, мозговой трест, желающее создать свободное общество посредством идей свободной рыночной экономики и индивидуализма.

We're a think tank who wants to shape a free society through ideas of free market economy and individualism.

Біз еркін нарықтық экономика мен индивидуализм идеялары арқылы еркін қоғам құрмақ болған талдау орталығымыз.

## what is libertarianism
Либертарианство это совокупность политических философий и движений, поддерживающие свободу как основной принцип. Либертарианцы заинтересованы в максимизации политических свобод и автономий, подчеркивая свободу выбора, добровольное объединение и индивидуализм.

Либертаршылық еркіндікті бас принцип ретінде қоятын саяси философиялар мен қозғалыстардың жиынтығы. Либертаршылар таңдау еркіндігін, ерікті бірлесуді, жекешілдікті негізге қойып саяси еркіндіктер мен автономияларды арттыруға ұмтылады.

Libertarianism is a set of political philosophies that upholds liberty as a core principle.  Libertarians seek to maximize political freedom and autonomy, emphasizing freedom of choice, voluntary association and individual judgement. 

## what do we do
Мы заинтересованы в __популяризации либертарианских идей__, посредством любых __образовательных материалов__, будь то это переводы статей, видео и тд. и тп..

Біз __либертар идеяларын__ қандай болмасын __оқу материалдары__ арқылы __дәріптеуді__ мақсат тұтатымыз. Оның түрі-түсінде шаруамыз жоқ: аудармалар, видео т.с.с..

We're __interested__ in the __popularization of libertarian ideas__ in whatever the educational way it's possible to do so. We don't care whether they're video, translations etc. .


## what we don't do
Мы __не политический актор__ и __не собираемся заниматься политикой__. Мы не представляем чьи-либо личные интересы. 

Біз ешқандай __саяси актор емеспіз__, __саясатпен айналысатын ойымыз жоқ__. Біз ешкімнің жеке мүддесін қорғап жүрген жоқпыз.

We're __not a political actor__ and we're __not involved in politics__. We're not representing anyone's personal interest.



## misc
 - add list of essential literature
 - add links to our soc networks and stuff 
